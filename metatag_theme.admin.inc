<?php

/**
 * @file
 * Admin pages
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2014 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form
 */
function metatag_theme_settings($form_id, $form_state, $theme) {
  $form = array();

  $form['metatag_theme_mobile_web_app_capable_' . $theme] = array(
    '#type' => 'checkbox',
    '#title' => t('mobile-web-app-capable'),
    '#default_value' => variable_get('metatag_theme_mobile_web_app_capable_' . $theme, FALSE),
  );

  $form['metatag_theme_apple_mobile_web_app_capable_' . $theme] = array(
    '#type' => 'checkbox',
    '#title' => t('apple-mobile-web-app-capable'),
    '#default_value' => variable_get('metatag_theme_apple_mobile_web_app_capable_' . $theme, FALSE),
  );

  $form['metatag_theme_apple_mobile_web_app_title_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('apple-mobile-web-app-title'),
    '#default_value' => variable_get('metatag_theme_apple_mobile_web_app_title_' . $theme, '[site:name]'),
  );

  $form['metatag_theme_x_ua_compatible_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('x-ua-compatible'),
    '#default_value' => variable_get('metatag_theme_x_ua_compatible_' . $theme, ''),
  );

  $form['metatag_theme_viewport_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('viewport'),
    '#default_value' => variable_get('metatag_theme_viewport_' . $theme, ''),
  );

  $form['metatag_theme_handheldfriendly_' . $theme] = array(
    '#type' => 'checkbox',
    '#title' => t('handheldfriendly'),
    '#default_value' => variable_get('metatag_theme_handheldfriendly_' . $theme, FALSE),
  );

  $form['metatag_theme_mobileoptimized_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('mobileoptimized'),
    '#description' => t('Screen width the website was developed for.'),
    '#default_value' => variable_get('metatag_theme_mobileoptimized_' . $theme, 0),
  );

  $form['metatag_theme_imagetoolbar_' . $theme] = array(
    '#type' => 'checkbox',
    '#title' => t('imagetoolbar'),
    '#default_value' => variable_get('metatag_theme_imagetoolbar_' . $theme, FALSE),
  );

  $form['metatag_theme_mssmarttagspreventparsing_' . $theme] = array(
    '#type' => 'checkbox',
    '#title' => t('mssmarttagspreventparsing'),
    '#default_value' => variable_get('metatag_theme_mssmarttagspreventparsing_' . $theme, FALSE),
  );

  $form['metatag_theme_cleartype_' . $theme] = array(
    '#type' => 'checkbox',
    '#title' => t('cleartype'),
    '#default_value' => variable_get('metatag_theme_cleartype_' . $theme, FALSE),
  );

  $form['favicon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Favicon'),
    '#description' => t('You may upload files to the public directory via !url.', array(
      '!url' => l(t('Content > Files'), 'admin/content/file'),
    )),
  );
  $form['favicon']['metatag_theme_favicon_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 16x16'),
    '#default_value' => variable_get('metatag_theme_favicon_' . $theme, ''),
  );

  $form['touch_icon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Touch icons'),
    '#description' => t('You may upload files to the public directory via !url.', array(
      '!url' => l(t('Content > Files'), 'admin/content/file'),
    )),
  );
  $form['touch_icon']['metatag_theme_touch_icon_57_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 57x57'),
    '#default_value' => variable_get('metatag_theme_touch_icon_57_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_72_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 72x72'),
    '#default_value' => variable_get('metatag_theme_touch_icon_72_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_76_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 76x76'),
    '#default_value' => variable_get('metatag_theme_touch_icon_76_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_114_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 114x114'),
    '#default_value' => variable_get('metatag_theme_touch_icon_114_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_120_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 120x120'),
    '#default_value' => variable_get('metatag_theme_touch_icon_120_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_144_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 144x144'),
    '#default_value' => variable_get('metatag_theme_touch_icon_144_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_152_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 152x152'),
    '#default_value' => variable_get('metatag_theme_touch_icon_152_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_180_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 180x180'),
    '#default_value' => variable_get('metatag_theme_touch_icon_180_' . $theme, ''),
  );

  $form['touch_icon']['metatag_theme_touch_icon_192_' . $theme] = array(
    '#type' => 'textfield',
    '#title' => t('Path to 192x192'),
    '#default_value' => variable_get('metatag_theme_touch_icon_192_' . $theme, ''),
  );

  $form['view']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['view']['token_help']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node'),
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the settings form
 */
function metatag_theme_settings_validate($form, &$form_state) {

}
